<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataCredito extends Model
{
	protected $table = 'data_credito';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'id',
    	'sucesos',
    	'soporte',
        'estado',
    	'id_departamento',
    	'id_municipio',
    ];

    protected $hidden = [
    ];
}
