<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model
{
    protected $table = 'departamentos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'iso',
    	'nombre',
    	'codigo',
    	'latitud',
    	'longitud',
    ];

    protected $hidden = [
    ];
}
