<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'id',
    	'nombre_empresa',
    	'accionistas',
    	'capital',
        'estado',
    	'id_tipo_empresa',
    	'id_departamento',
    	'id_municipio',
        'id_empresa'
    ];

    protected $hidden = [
    ];
}
