<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
	protected $table = 'register';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nombre',
    	'apellido',
    	'tipo_doc',
    	'documento',
    	'email',
    	'telefono',
    	'password',
        'hash',
    ];

    protected $hidden = [
    	'password',
    ];
}
