<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Municipios extends Model
{
	protected $table = 'municipios';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'iso',
    	'nombre',
    	'codigo',
    	'codigo_departamento',
    	'latitud',
    	'longitud',
    ];

    protected $hidden = [
    ];
}
