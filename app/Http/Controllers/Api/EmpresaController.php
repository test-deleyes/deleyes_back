<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    var $rules = [
        'nombre_empresa' => 'required|string',
        'accionistas' => 'required|string',
        'capital' => 'required|string',
        'estado' => 'required|integer',
        'id_tipo_empresa' => 'required||exists:tipo_empresa,id',
        'id_departamento' => 'required|exists:departamentos,id',
        'id_municipio' => 'required|exists:municipios,id',
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = Empresa::orderBy('nombre_empresa')->get()->toArray();
        return response()->json($empresas, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $valid = Validator::make($request->all(), $this->rules);

        if ($valid->fails()) {
            return response()->json(['error'=>$valid->errors()], 200);
        }

        $empresa = new Empresa($request->all());

        if(!$empresa->save())
        {
            return response()->json(['error'=>'No se pudo crear la empresa', 'status'=> false], 200);
        }

        return response()->json(['status'=> true, 'data' => $empresa], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
