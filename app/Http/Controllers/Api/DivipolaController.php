<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Departamentos;
use App\Models\Municipios;
//use Illuminate\Http\Request;
use Validator;

class DivipolaController extends Controller
{
	public function departamentos()
	{
		$departamentos = Departamentos::get();

		$response = [
			'status' => true,
			'departamentos' => $departamentos,
		];

		return response()->json($response, 200);
	}

	public function municipios($codigo = null)
	{
		$data = ['codigo_departamento' => $codigo];

		$rules = [
			'codigo_departamento' => [
				'exists:departamentos,codigo',
			]
		];

		$message = [
			"codigo_departamento.exists" => "El :attribute no es válido",
		];

		$validator = Validator::make($data, $rules, $message);

		if ($validator->fails()) {
			$response = [
				'status' => false,
				'error'=>$validator->errors()
			];

			return response()->json($response, 200);
		}

		$municipios = Municipios::where('codigo_departamento', $codigo)->get();

		if (count($municipios) == 0){
			$municipios = [
				[
					"id" => 1,
					"iso" => null,
					"nombre" => "Municipo 1",
					"codigo" => "27006",
					"codigo_departamento" => $codigo,
					"latitud" => "8.3333333",
					"longitud" => "-77.1666667",
					"created_at" => null,
					"updated_at" => null
				],
				[
					"id" => 2,
					"iso" => null,
					"nombre" => "Municipo 2",
					"codigo" => "27007",
					"codigo_departamento" => $codigo,
					"latitud" => "8.3333333",
					"longitud" => "-77.1666667",
					"created_at" => null,
					"updated_at" => null
				],
				[
					"id" => 3,
					"iso" => null,
					"nombre" => "Municipo 3",
					"codigo" => "27008",
					"codigo_departamento" => $codigo,
					"latitud" => "8.3333333",
					"longitud" => "-77.1666667",
					"created_at" => null,
					"updated_at" => null
				]
			];
		}

		$response = [
			'status' => true,
			'municipios' => $municipios,
			'codigo' => $codigo,
		];

		return response()->json($response, 200);
	}
}
