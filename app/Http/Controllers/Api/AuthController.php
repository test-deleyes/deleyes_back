<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Register;
use App\Rules\has;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{



	public function register(Request $request)
	{

		$dataRegister = $request->all();

		$rulesRegister = [
			'nombre' => 'required',
			'apellido' => 'required',
			'tipo_doc' => 'required',
			'documento' => 'required',
			'email' => [
				'required',
				'unique:register,email'
			],
			'telefono' => 'required',
			'password' => 'required',
			'confirm_password' => 'required|same:password',
		];

		$messageRegister = [
			"nombre.required" => "El campo :attribute es requirido",
			"apellido.required" => "El campo :attribute es requirido",
			"tipo_doc.required" => "El campo :attribute es requirido",
			"documento.required" => "El campo :attribute es requirido",
			"email.required" => "El campo :attribute es requirido",
			"email.unique" => "El :attribute ya fue registrado",
			"telefono.required" => "El campo :attribute es requirido",
			"password.required" => "El campo :attribute es requirido",
			"confirm_password.required" => "El campo :attribute es requirido",
		];

		$validator = Validator::make($dataRegister, $rulesRegister, $messageRegister);

		if ($validator->fails()) {
			$response = [
				'status' => false,
				'error'=>$validator->errors(),
			];

			return response()->json($response, 200);
		}

		$input = $request->all();
		$input['hash'] = md5($request->get('email'));
		$user = Register::create($input);

		$response = [
			'status' => true,
			'user' => $user,
		];

		return response()->json($response, 200);
	}

	public function validateRegister($hash = null)
	{

		$data = ['hash' => $hash];

		$rules = [
			'hash' => [
				'size:32',
				'exists:register,hash',
			]
		];

		$message = [
			"hash.size" => "El :attribute debe tener :size carácteres",
			"hash.exists" => "El :attribute no es válido",
		];

		$validator = Validator::make($data, $rules, $message);

		if ($validator->fails()) {
			$response = [
				'status' => false,
				'error'=>$validator->errors()
			];

			return response()->json($response, 200);
		}

		$registerData = Register::where('hash', $hash)->first();

		$input = [
			'name' => $registerData['nombre'] . ' ' . $registerData['apellido'],
			'email' => $registerData['email'],
			'password' => bcrypt($registerData['password']),
			'rol' => 1,
		];


		$data = ['email' => $input['email']];

		$rules = [
			'email' => [
				'unique:users,email',
			]
		];

		$message = [
			"email.unique" => "el usuario ya está registrado",
		];

		$validatorUser = Validator::make($data, $rules, $message);

		if ($validatorUser->fails()) {
			$response = [
				'status' => false,
				'error'=>$validatorUser->errors()
			];

			return response()->json($response, 200);
		}

		$user = User::create($input);
		$token =  $user->createToken('MyApp')->accessToken;

		$response = [
			'status' => true,
			'user' => $user,
			'token' => $token,
		];

		return response()->json($response, 200);
	}

	public function login(Request $request)
	{
		if (Auth::attempt($request->only('email', 'password')))
		{
			$user = Auth::user();
			$token =  $user->createToken('MyApp')->accessToken;

			$response = [
				'status' => true,
				'token' => $token,
				'user' => $user,
				'url_frontend' => env('FRONT_URL', 'http://localhost:4200'),
			];
			return response()->json($response, 200);
		}
		else
		{
			$response = [
				'status' => false,
				'message' => 'unauthorized',
				'error' => 401,
			];
			return response()->json($response, 200);
		}
	}

	public function profile()
	{
		$user = Auth::user();
		return response()->json(compact('user'), 200);
	}

/*	public function readtoken(Request $request)
	{
		$hash = $request->get('hash');
		$user_id = $request->get('user_id');

		$userData = FdFirmasUsuarios::where('usuario_id',$user_id)->first();

		$user = FdFirmasUsuarios::where([['tipo_usuario',1],['usuario_id',$user_id]])->first();
		if(!empty($user) && $user->tipo_usuario == 1)
			$tipo = ['US'];

		$user = FdFirmasUsuarios::where([['tipo_usuario',2],['usuario_id',$user_id]])->first();
		if(!empty($user) && $user->tipo_usuario == 2)
			$tipo = ['FI'];

		$adminIds = FdFirmasUsuarios::where([['tipo_usuario', 0]])->get()->pluck('usuario_id')->toArray();

		if(in_array($user_id, $adminIds))
			$tipo [] = 'AD';

		if($hash != null){

			$redis = Redis::connection();

			if($redis->exists($hash)){
				$token = $redis->get($hash);
				$response = [
					"token" => $redis->get($hash),
					"hash" => $hash,
					"user" => [
						'name' => 'Orfeo Services',
						'email' => 'orfeoservices@superservicios.gov.org',
						"id" => $user_id,
						"rol" => $tipo
					],
					"user_data" => $userData,
					'url_frontend' => env('FRONT_URL', 'http://localhost:4200'),
				];
				return response()->json($response, 200);
			}else{
				return response()->json(['error' => 'value no exists'], 401);
			}
		}else{
			return response()->json(['error' => 'The value was not sent'], 401);
		}

	}*/
}
