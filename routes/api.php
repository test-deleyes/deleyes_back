<?php

// use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function(){
	Route::post('login', 'Api\AuthController@login');
	Route::post('register', 'Api\AuthController@register');
	Route::get('validate/{hash?}', 'Api\AuthController@validateRegister')->name('validate');
});

Route::prefix('v1/get')->middleware(['auth:api', 'cors'])->group(function(){
	Route::get('profile', 'Api\AuthController@profile')->name('profile');

	Route::get('departamentos', 'Api\DivipolaController@departamentos')->name('departamentos');
	Route::get('municipios/{codigo?}', 'Api\DivipolaController@municipios')->name('municipios');
});


Route::prefix('v1/resources')->middleware(['auth:api', 'cors'])->group(function(){
	Route::resources([
		'empresas' => 'Api\EmpresaController',
		'datacredito' => 'Api\DataCreditoController'
	]);
});

