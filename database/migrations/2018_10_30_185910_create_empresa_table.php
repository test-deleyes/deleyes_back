<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_empresa');
            $table->string('accionistas');
            $table->bigInteger('capital');
            $table->integer('estado');
            $table->integer('id_tipo_empresa')->unsigned();
            $table->integer('id_departamento')->unsigned();
            $table->integer('id_municipio')->unsigned();
            $table->timestamps();
        });

        Schema::table('empresa', function(Blueprint $table) {
            $table->foreign('id_tipo_empresa')->references('id')->on('tipo_empresa')->onDelete('cascade');
            $table->foreign('id_departamento')->references('id')->on('departamentos')->onDelete('cascade');
            $table->foreign('id_municipio')->references('id')->on('municipios')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa');
    }
}
