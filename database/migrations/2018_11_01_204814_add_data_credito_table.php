<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataCreditoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_credito', function (Blueprint $table) {
            $table->integer('id_empresa')->unsigned();
        });

        Schema::disableForeignKeyConstraints();
        Schema::table('data_credito', function (Blueprint $table) {
            $table->foreign('id_empresa')->references('id')->on('empresa')->onDelete('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_credito', function (Blueprint $table) {
            //
        });
    }
}
