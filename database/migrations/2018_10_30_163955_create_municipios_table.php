<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('iso')->nullable();
            $table->string('nombre');
            $table->string('codigo');
            $table->string('codigo_departamento');
            $table->string('latitud');
            $table->string('longitud');
            $table->timestamps();
        });

        Schema::table('municipios', function(Blueprint $table) {
            $table->foreign('codigo_departamento')->references('codigo')->on('departamentos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('municipios');
    }
}
