<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataCreditoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_credito', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sucesos');
            $table->text('soporte');
            $table->integer('estado');
            $table->integer('id_departamento')->unsigned();
            $table->integer('id_municipio')->unsigned();
            $table->timestamps();
        });

        Schema::table('data_credito', function(Blueprint $table) {
            $table->foreign('id_departamento')->references('id')->on('departamentos')->onDelete('cascade');
            $table->foreign('id_municipio')->references('id')->on('municipios')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_credito');
    }
}
