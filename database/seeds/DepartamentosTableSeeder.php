<?php

use Illuminate\Database\Seeder;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$data = [
    		[
    			'iso' => 'CO-AMA',
    			'nombre' => 'Amazonas',
    			'codigo' => '91',
    			'latitud' => '-1.0197222',
    			'longitud' => '-71.9383333',
    		],
    		[
    			'iso' => 'CO-ANT',
    			'nombre' => 'Antioquia',
    			'codigo' => '05',
    			'latitud' => '7',
    			'longitud' => '-75.5',
    		],
    		[
    			'iso' => 'CO-ARA',
    			'nombre' => 'Arauca',
    			'codigo' => '81',
    			'latitud' => '7.0902778',
    			'longitud' => '-70.7616667',
    		],
    		[
    			'iso' => 'CO-ATL',
    			'nombre' => 'Atlántico',
    			'codigo' => '08',
    			'latitud' => '10.75',
    			'longitud' => '-75',
    		],
    		[
    			'iso' => 'CO-BOL',
    			'nombre' => 'Bolívar',
    			'codigo' => '13',
    			'latitud' => '9',
    			'longitud' => '-74.3333333',
    		],
    		[
    			'iso' => 'CO-BOY',
    			'nombre' => 'Boyacá',
    			'codigo' => '15',
    			'latitud' => '5.5',
    			'longitud' => '-72.5',
    		],
    		[
    			'iso' => 'CO-CAL',
    			'nombre' => 'Caldas',
    			'codigo' => '17',
    			'latitud' => '5.25',
    			'longitud' => '-75.5',
    		],
    		[
    			'iso' => 'CO-CAQ',
    			'nombre' => 'Caquetá',
    			'codigo' => '18',
    			'latitud' => '1',
    			'longitud' => '-74',
    		],
    		[
    			'iso' => 'CO-CAS',
    			'nombre' => 'Casanare',
    			'codigo' => '85',
    			'latitud' => '5.5',
    			'longitud' => '-71.5',
    		],
    		[
    			'iso' => 'CO-CAU',
    			'nombre' => 'Cauca',
    			'codigo' => '19',
    			'latitud' => '2.5',
    			'longitud' => '-76.8333333',
    		],
    		[
    			'iso' => 'CO-CES',
    			'nombre' => 'Cesar',
    			'codigo' => '20',
    			'latitud' => '9.3333333',
    			'longitud' => '-73.5',
    		],
    		[
    			'iso' => 'CO-CHO',
    			'nombre' => 'Chocó',
    			'codigo' => '27',
    			'latitud' => '6',
    			'longitud' => '-77',
    		],
    		[
    			'iso' => 'CO-COR',
    			'nombre' => 'Cundinamarca',
    			'codigo' => '25',
    			'latitud' => '5',
    			'longitud' => '-74.1666667',
    		],
    		[
    			'iso' => 'CO-CUN',
    			'nombre' => 'Córdoba',
    			'codigo' => '23',
    			'latitud' => '8.3333333',
    			'longitud' => '-75.6666667',
    		],
    		[
    			'iso' => 'CO-GUA',
    			'nombre' => 'Guainía',
    			'codigo' => '94',
    			'latitud' => '2.5',
    			'longitud' => '-69',
    		],
    		[
    			'iso' => 'CO-GUV',
    			'nombre' => 'Guaviare',
    			'codigo' => '95',
    			'latitud' => '1.6894444',
    			'longitud' => '-72.8202778',
    		],
    		[
    			'iso' => 'CO-HUI',
    			'nombre' => 'Huila',
    			'codigo' => '41',
    			'latitud' => '2.5',
    			'longitud' => '-75.75',
    		],
    		[
    			'iso' => 'CO-LAG',
    			'nombre' => 'La Guajira',
    			'codigo' => '44',
    			'latitud' => '11.5',
    			'longitud' => '-72.5',
    		],
    		[
    			'iso' => 'CO-MAG',
    			'nombre' => 'Magdalena',
    			'codigo' => '47',
    			'latitud' => '10',
    			'longitud' => '-74.5',
    		],
    		[
    			'iso' => 'CO-MET',
    			'nombre' => 'Meta',
    			'codigo' => '50',
    			'latitud' => '3.5',
    			'longitud' => '-73',
    		],
    		[
    			'iso' => 'CO-NAR',
    			'nombre' => 'Nariño',
    			'codigo' => '52',
    			'latitud' => '1.5',
    			'longitud' => '-78',
    		],
    		[
    			'iso' => 'CO-NSA',
    			'nombre' => 'Norte de Santander',
    			'codigo' => '54',
    			'latitud' => '8',
    			'longitud' => '-73',
    		],
    		[
    			'iso' => 'CO-PUT',
    			'nombre' => 'Putumayo',
    			'codigo' => '86',
    			'latitud' => '0.5',
    			'longitud' => '-76',
    		],
    		[
    			'iso' => 'CO-QUI',
    			'nombre' => 'Quindío',
    			'codigo' => '63',
    			'latitud' => '4.5',
    			'longitud' => '-75.6666667',
    		],
    		[
    			'iso' => 'CO-RIS',
    			'nombre' => 'Risaralda',
    			'codigo' => '66',
    			'latitud' => '5',
    			'longitud' => '-76',
    		],
    		[
    			'iso' => 'CO-SAP',
    			'nombre' => 'San Andrés',
    			'codigo' => '88',
    			'latitud' => '12.5847222',
    			'longitud' => '-81.7005556',
    		],
    		[
    			'iso' => 'CO-SAN',
    			'nombre' => 'Santander',
    			'codigo' => '68',
    			'latitud' => '7',
    			'longitud' => '-73.25',
    		],
    		[
    			'iso' => 'CO-SUC',
    			'nombre' => 'Sucre',
    			'codigo' => '70',
    			'latitud' => '9',
    			'longitud' => '-75',
    		],
    		[
    			'iso' => 'CO-TOL',
    			'nombre' => 'Tolima',
    			'codigo' => '73',
    			'latitud' => '3.75',
    			'longitud' => '-75.25',
    		],
    		[
    			'iso' => 'CO-VAC',
    			'nombre' => 'Valle del Cauca',
    			'codigo' => '76',
    			'latitud' => '3.75',
    			'longitud' => '-76.5',
    		],
    		[
    			'iso' => 'CO-VAU',
    			'nombre' => 'Vaupés',
    			'codigo' => '97',
    			'latitud' => '0.25',
    			'longitud' => '-70.75',
    		],
    		[
    			'iso' => 'CO-VID',
    			'nombre' => 'Vichada',
    			'codigo' => '99',
    			'latitud' => '5',
    			'longitud' => '-69.5',
    		],
    	];

    	foreach ($data as $row) {
    		DB::table('departamentos')->insert($row);
    	}
    }
}
