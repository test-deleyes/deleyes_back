<?php

use Illuminate\Database\Seeder;

class DataCreditoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
    		[
    			'sucesos' => 'Data crédito',
    			'soporte' => 'Algún archivo',
    			'estado' => 1,
    			'id_departamento' => 1,
    			'id_municipio' => 1,
                'id_empresa' => 1,
    		],
    	];

    	foreach ($data as $row) {
    		DB::table('data_credito')->insert($row);
    	}
    }
}
