<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

    	$tables = [
    		'departamentos',
    		'municipios',
            'tipo_empresa',
            'empresa',
            'data_credito',
        ];

        $this->truncateTables($tables);

        // $this->call(UsersTableSeeder::class);
        $this->call(DepartamentosTableSeeder::class);
        $this->call(MunicipiosTableSeeder::class);
        $this->call(TipoEmpresaTableSeeder::class);
        $this->call(EmpresaTableSeeder::class);
        $this->call(DataCreditoTableSeeder::class);

    }

    /**
     * Metodo para vaciar las tablas de informacion,
     * se debe validar el tipo de conección para la elimincacion de la información
     *
     *     eliminacion de mysql utiliza               => truncate()
     *     eliminacion de postgres utiliza            => delete()
     *
     * @param  array  $tables [listado de tablas]
     * @return [void]         [description]
     */
    protected function truncateTables(array $tables)
    {

    	Schema::disableForeignKeyConstraints();

    	foreach ($tables as $table) {
    		if (env('DB_CONNECTION') == "mysql") {
    			DB::table($table)->truncate();
    		}

    		if (env('DB_CONNECTION') == "pgsql") {
    			DB::table($table)->delete();
    		}

    		echo "Truncate table: " . $table . "\n";
    	}

    	Schema::enableForeignKeyConstraints();
    }
}
