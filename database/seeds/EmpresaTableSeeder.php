<?php

use Illuminate\Database\Seeder;

class EmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
    		[
    			'nombre_empresa' => 'Creación de empresa',
    			'accionistas' => 'Algún accionista',
    			'capital' => 1320000,
    			'estado' => 1,
    			'id_tipo_empresa' => 1,
    			'id_departamento' => 1,
    			'id_municipio' => 1,
    		],
    	];

    	foreach ($data as $row) {
    		DB::table('empresa')->insert($row);
    	}
    }
}
