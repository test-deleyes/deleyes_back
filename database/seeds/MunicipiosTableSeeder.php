<?php

use Illuminate\Database\Seeder;

class MunicipiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
    		[
    			'nombre' => 'Abejorral',
    			'codigo' => '05002',
    			'codigo_departamento' => '05',
    			'latitud' => '5.75',
    			'longitud' => '-75.4166667',
    		],
    		[
    			'nombre' => 'Abriaquí',
    			'codigo' => '05004',
    			'codigo_departamento' => '05',
    			'latitud' => '6.6666667',
    			'longitud' => '-76.0833333',
    		],
    		[
    			'nombre' => 'Acacías',
    			'codigo' => '50006',
    			'codigo_departamento' => '50',
    			'latitud' => '3.9166667',
    			'longitud' => '-73.8333333',
    		],
    		[
    			'nombre' => 'Acandí',
    			'codigo' => '27006',
    			'codigo_departamento' => '27',
    			'latitud' => '8.3333333',
    			'longitud' => '-77.1666667',
    		],
    		[
    			'nombre' => 'Acevedo',
    			'codigo' => '41006',
    			'codigo_departamento' => '41',
    			'latitud' => '1.75',
    			'longitud' => '-75.9166667',
    		],
    		[
    			'nombre' => 'Achí',
    			'codigo' => '13006',
    			'codigo_departamento' => '13',
    			'latitud' => '8.25',
    			'longitud' => '-74.5',
    		],
    		[
    			'nombre' => 'Agrado',
    			'codigo' => '41013',
    			'codigo_departamento' => '41',
    			'latitud' => '2.3333333',
    			'longitud' => '-75.75',
    		],
    		[
    			'nombre' => 'Agua de Dios',
    			'codigo' => '25001',
    			'codigo_departamento' => '25',
    			'latitud' => '4.4166667',
    			'longitud' => '-74.6666667',
    		],
    		[
    			'nombre' => 'Aguachica',
    			'codigo' => '20011',
    			'codigo_departamento' => '20',
    			'latitud' => '8.3333333',
    			'longitud' => '-73.5833333',
    		],
    		[
    			'nombre' => 'Aguada',
    			'codigo' => '68013',
    			'codigo_departamento' => '68',
    			'latitud' => '6.25',
    			'longitud' => '-73.4666667',
    		],
    		[
    			'nombre' => 'Aguadas',
    			'codigo' => '17013',
    			'codigo_departamento' => '17',
    			'latitud' => '5.6333333',
    			'longitud' => '-75.4166667',
    		],
    		[
    			'nombre' => 'Aguazul',
    			'codigo' => '85010',
    			'codigo_departamento' => '85',
    			'latitud' => '5.1730556',
    			'longitud' => '-72.5547222',
    		],
    		[
    			'nombre' => 'Agustín',
    			'codigo' => '20013',
    			'codigo_departamento' => '20',
    			'latitud' => '9.9166667',
    			'longitud' => '-73.25',
    		],
    		[
    			'nombre' => 'Aipe',
    			'codigo' => '41016',
    			'codigo_departamento' => '41',
    			'latitud' => '3.25',
    			'longitud' => '-75.3333333',
    		],
    		[
    			'nombre' => 'Albania',
    			'codigo' => '18029',
    			'codigo_departamento' => '18',
    			'latitud' => '1.3316667',
    			'longitud' => '-75.8822222',
    		],
    		[
    			'nombre' => 'Albania',
    			'codigo' => '44035',
    			'codigo_departamento' => '44',
    			'latitud' => '11.1597222',
    			'longitud' => '-72.5855556',
    		],
    		[
    			'nombre' => 'Albania',
    			'codigo' => '68020',
    			'codigo_departamento' => '68',
    			'latitud' => '5.8333333',
    			'longitud' => '-73.75',
    		],
    		[
    			'nombre' => 'Albán',
    			'codigo' => '25019',
    			'codigo_departamento' => '25',
    			'latitud' => '4.9166667',
    			'longitud' => '-74.45',
    		],
    		[
    			'nombre' => 'Albán',
    			'codigo' => '52019',
    			'codigo_departamento' => '52',
    			'latitud' => '1.4744444',
    			'longitud' => '-77.0836111',
    		],
    		[
    			'nombre' => 'Alcalá',
    			'codigo' => '76020',
    			'codigo_departamento' => '76',
    			'latitud' => '4.6666667',
    			'longitud' => '-75.75',
    		],
    		[
    			'nombre' => 'Aldana',
    			'codigo' => '52022',
    			'codigo_departamento' => '52',
    			'latitud' => '0.9166667',
    			'longitud' => '-77.6833333',
    		],
    		[
    			'nombre' => 'Alejandria',
    			'codigo' => '05021',
    			'codigo_departamento' => '05',
    			'latitud' => '6.3666667',
    			'longitud' => '-75.0833333',
    		],
    		/*[
    			'nombre' => 'name',
    			'codigo' => 'code',
    			'codigo_departamento' => 'cod_dpt',
    			'latitud' => 'lat',
    			'longitud' => '-long',
    		],*/

    	];

    	foreach ($data as $row) {
    		DB::table('municipios')->insert($row);
    	}
    }
}
