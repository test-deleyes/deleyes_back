<?php

use Illuminate\Database\Seeder;

class TipoEmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
    		[
    			'tipo' => 'SA',
    		],
    		[
    			'tipo' => 'SAS',
    		],
    		[
    			'tipo' => 'LTDA',
    		],
    	];

    	foreach ($data as $row) {
    		DB::table('tipo_empresa')->insert($row);
    	}
    }
}
