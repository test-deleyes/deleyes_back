<?php

 /*
	 |--------------------------------------------------------------------------
	 | Laravel CORS - https://github.com/barryvdh/laravel-cors
	 |--------------------------------------------------------------------------
	 |
	 | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
	 | to accept any value.
	 |
*/

// to publish -- php artisan vendor:publish --provider="Barryvdh\Cors\ServiceProvider"

 return [
 	'supportsCredentials' => false,
 	'allowedOrigins' => ['*'],
 	'allowedHeaders' => ['*'],
	'allowedMethods' => ['*'], // ex: ['GET', 'POST', 'PUT',  'DELETE']
	'exposedHeaders' => [],
	'maxAge' => 0,
];